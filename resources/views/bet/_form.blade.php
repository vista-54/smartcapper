<div class="form-group">
    {!!Form::label('title', 'Основное') !!}
    {!!Form::text('title', null, ['class' => 'form-control']) !!}
    <div>
        {!!Form::label('body', 'Аналитика') !!}
        <br>
        {{ Form::textarea('body') }}
    </div>
    {{ Form::select('status', ['', 'win', 'lose','refund','pending']) }}
    <br>
    {!!Form::label('calendar1', 'Время начала') !!}
    {!!Form::text('start_date', null, ["placeholder" => "2014-09-09 12:00", 'id' => 'calendar1']) !!}
    <br>
    {!!Form::label('coefficient', 'Коэфициент') !!}
    {!!Form::text('coefficient', null, []) !!}
    <br>
    {!!Form::label('screen', 'Скрин шот(ссылка)') !!}
    {!!Form::text('screen', null, ['class' => 'form-control']) !!}
</div>
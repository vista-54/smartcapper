@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Bets</div>

                    <div class="panel-body">
                        {{ link_to_route('bet.create', 'create', null, ['class' => 'btn btn-info btn-xs']) }}
                        <table class="table table-bordered table-responsive table-striped">
                            <tr>
                                <th>id</th>
                                <th>Title</th>
                                <th>Body</th>
                                <th>Start</th>
                                <th>Coefficient</th>
                                <th>Screen link</th>
                                <th>Status</th>
                                <th>Published</th>
                                <th>Actions</th>
                            </tr>

                            @foreach ($bets as $model)
                                <tr>
                                    <td>{{$model->id}}</td>
                                    <td>{{$model->title}}</td>
                                    <td>{{$model->body}}</td>
                                    <td>{{$model->start_date}}</td>
                                    <td>{{$model->coefficient}}</td>
                                    <td>{{$model->screen}}</td>
                                    <td>{{$model->status}}</td>
                                    @if ($model->published)
                                        <td>Да</td>
                                    @else
                                        <td>Нет</td>
                                    @endif
                                    <td>
                                        {{Form::open(['class' => 'confirm-delete', 'route' => ['bet.destroy', $model->id], 'method' => 'DELETE'])}}
                                        {{ link_to_route('bet.show', 'info', [$model->id], ['class' => 'btn btn-info btn-xs']) }}
                                        |
                                        {{ link_to_route('bet.edit', 'edit', [$model->id], ['class' => 'btn btn-success btn-xs']) }}
                                        | @if (!$model->published)
                                            {{ link_to_route('bet.publish', 'publish', [$model->id], ['class' => 'btn btn-success btn-xs','name'=>'published']) }}
                                        @endif
                                        {{Form::button('Delete', ['class' => 'btn btn-danger btn-xs', 'type' => 'submit'])}}
                                        {{Form::close()}}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

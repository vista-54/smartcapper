<?php

namespace App\Library\Services;

use App\models\Bet;
use App\models\Client;
use App\models\Payment;
use Mockery\Exception;
use Telegram\Bot\Exceptions\TelegramResponseException;
use Telegram\Bot\Keyboard\Keyboard;

/**
 * Created by PhpStorm.
 * User: vista
 * Date: 09.03.18
 * Time: 00:18
 */
class TelegramBot
{

    private $telegram;
    private $chat_id;
    private $name;

    const MERCHANT_ID = '69802';
    const SECRET = 'da84vqql';

    public function __construct()
    {
        $this->telegram = new \Telegram\Bot\Api('406621208:AAHag4Hm_FE2rL4PjppLexuV2aTDJCzPfIs'); //Устанавливаем токен, полученный у BotFather
    }

    public function run()
    {
        $result = $this->telegram->getWebhookUpdate(); //Передаем в переменную $result полную информацию о сообщении пользователя
        $file = 'people.txt';
// Открываем файл для получения существующего содержимого
        $current = file_get_contents($file);
// Добавляем нового человека в файл
        $current .= json_encode($result, JSON_UNESCAPED_UNICODE) . "\n";
// Пишем содержимое обратно в файл
        file_put_contents($file, $current);
        $text = mb_convert_encoding($result["message"]["text"], "UTF-8");
        $this->chat_id = $result["message"]["chat"]["id"]; //Уникальный идентификатор пользователя
        if (!isset($result["message"]["from"]["username"])) {
            $this->name = $result["message"]["from"]["first_name"]; //имя пользователя
        }

        $this->createClient($result['message']['from']);
        if ($text) {
            if ($text == "/start" || $text == "Главное меню") {
                $this->start();
            } elseif ($text == "Статистика") {
                $this->getHistory();
            } elseif ($text == "Ставка дня") {
                $this->betOfDay();
            } elseif ($text == "О проекте") {
                $this->aboutProject();
            } elseif ($text == "Купить") {
                $this->buy();
            } else {
                $this->telegram->sendMessage(['chat_id' => $this->chat_id, 'text' => "Команда" . $text . " не найдена."]);
            }
        }
    }


    private function start()
    {
        $keyboard = [["Ставка дня"], ["Статистика"], ["О проекте"]];
        $reply = "Добро пожаловать в Smart Capper, " . $this->name;
        $reply_markup = $this->telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
        $this->telegram->sendMessage(['chat_id' => $this->chat_id, 'text' => $reply, 'reply_markup' => $reply_markup]);
    }

    private function getHistory()
    {
        $betsCount = Bet::where('status', '!=', '')->count();
        $bet_win = Bet::where(['status' => 'win'])->count();
        $bet_lose = Bet::where(['status' => 'lose'])->count();
        $bet_refund = Bet::where(['status' => 'refund'])->count();

        $keyboard = [["Ставка дня"], ["Статистика"], ["О проекте"]]; //Клавиатура

        $title = "За последний месяц сделано ставок: " . $betsCount;
        $title .= " Побед: " . $bet_win;
        $title .= " Проигрышей: " . $bet_lose;
        $title .= " Возвратов: " . $bet_refund;
        $this->telegram->sendMessage(['chat_id' => $this->chat_id, 'text' => $title]);
        $bets = Bet::where('status', '!=', '')->orderBy('id', 'ask')->take(3)->get();
        foreach ($bets as $item) {
            $screenshot = $item['screen'];
            $reply_markup = $this->telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
            $this->telegram->sendMessage(['chat_id' => $this->chat_id, 'text' => $screenshot, 'reply_markup' => $reply_markup]);
        }
    }

    private function betOfDay()
    {
        $bet = Bet::where(['status' => ''])->first();
        if (!count($bet)) {
            $keyboard = [["Ставка дня"], ["Статистика"], ["О проекте"]];
            $reply_markup = $this->telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
            $this->telegram->sendMessage(['chat_id' => $this->chat_id, 'text' => "На сегодня ставок нет. Попробуйте позже", 'reply_markup' => $reply_markup]);
        } else {
            $client = Client::where(['tg_chat_id' => $this->chat_id])->first();
            $payment = Payment::where(['bet_id' => $bet->id, 'client_id' => $client->id])->count();
            $keyboard = [["Купить"], ["Статистика"], ['Главное меню']];
            $text = $bet->start_date . " состоиться матч с кф. " . $bet->coefficient;
            if ($payment) {
                $keyboard = [["Статистика"], ['Главное меню']];

                $text = "Вы купили актуальный прогноз. О выходе следующего вы будете проинформированны дополнительно";
            }
            $reply_markup = $this->telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);

            $this->telegram->sendMessage(['chat_id' => $this->chat_id, 'text' => $text, 'reply_markup' => $reply_markup]);
        }
    }

    private function aboutProject()
    {
        $keyboard = [["Ставка дня"], ["Статистика"], ["О проекте"]];
        $aboutUsText = "    Smart Capper создан с целью помочь пользователям в мире ставок, получить не только удовольствия от беттинга, а и заработать денег.
        Я воспринимаю ставки как инвестиции. У меня есть стратегия игры, по этом 
        я всегда в плюсе.Разница между профессионалом и аматором в том, что последний не сможет повторить успех вновь.
        А профессионализм в беттинге это доход на дистанции.\n
        Если у вас возникли проблемы в использовании бота, или другие вопросы, обратитесь на нашу эл. почту указанную в описании группы";
        $reply_markup = $this->telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
        $this->telegram->sendMessage(['chat_id' => $this->chat_id, 'text' => $aboutUsText, 'reply_markup' => $reply_markup]);
    }

    /**
     * Регистрируем нового клиента
     * @param $data
     */
    private function createClient($data)
    {
        $tmpObject = new Client();
        if (!Client::where(['tg_chat_id' => $this->chat_id])->count()) {
            $tmpObject['tg_chat_id'] = $this->chat_id;
            $tmpObject['tg_username'] = isset($data['username']) ? $data['username'] : $data['first_name'];
            $tmpObject['tg_first_name'] = isset($data['first_name']) ? $data['first_name'] : '';
            $tmpObject['tg_last_name'] = isset($data['last_name']) ? $data['last_name'] : '';
            $tmpObject->save();
        }
    }

    private function buy()
    {
        $currentBet = Bet::where(['status' => ''])->get();
        if (count($currentBet) == 0) {
            $this->telegram->sendMessage(['chat_id' => $this->chat_id, 'text' => "На сегодня ставок нет. Попробуйте позже"]);
            die();
        }
        $merchant_id = self::MERCHANT_ID;
        $secret_word = self::SECRET;
        $order_id = $this->chat_id . "b" . $currentBet[0]->id;
        $order_amount = '299';
        $sign = md5("$merchant_id" . ':' . $order_amount . ':' . $secret_word . ':' . $order_id);

        $url = "http://www.free-kassa.ru/merchant/cash.php?";
        $url .= "m=" . $merchant_id . "&oa=" . $order_amount . "&o=" . $order_id . "&s=" . $sign;
        $btn = Keyboard::inlineButton(
            ['text' => "Оплатить (299 р.)",
                'url' => $url]
        );
        $keyboard = Keyboard::make([[$btn]]);
        $reply_markup = $this->telegram->replyKeyboardMarkup(['inline_keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
        $this->telegram->sendMessage(['chat_id' => $this->chat_id, 'text' => "Совершите оплату, и сразу получите личное сообщение с деталями прогноза. Если ставка не сыграет, следующую получите бесплатно.", 'reply_markup' => $reply_markup]);

    }

    public function sendNewBetToClients($betId)
    {
        $bet = Bet::find($betId);
        $keyboard = [["Ставка дня"], ["Статистика"], ["О проекте"]];
        $reply_markup = $this->telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
        //взять последнюю платную ставку, проверить не слила ли она,
        $clientsWithPayments = [];
        $last_bets = Bet::where('status', '!=', '')->orderBy('id', 'desc')->first();
        if ($last_bets['status'] == 'lose') {
            $payments = Payment::where(['bet_id' => $last_bets['id']])->get();//проверяем кто ее покупал
            foreach ($payments as $pay) {
                $clientsWithPayments[] = $pay->client_id;//собираем айдишники всех юзеров
            }
        }
        $clients = Client::all();

        foreach ($clients as $client) {

            try {
                $text = "Здравствуй, " . $client->tg_username . ", в раздел 'Ставка дня' добавлен прогноз с кф. " . $bet->coefficient . ", начало матча в " . $bet->start_date . " по Мск";
                $this->telegram->sendMessage(['chat_id' => $client->tg_chat_id, 'text' => $text, 'reply_markup' => $reply_markup]);

            } catch (TelegramResponseException $e) {

                $errorData = $e->getResponseData();

                if ($errorData['ok'] === false) {

                }
            }
        }
        if (count($clientsWithPayments)) {//если хоть 1 есть, то рассылаем им бесплатно
            $clientsWithPay = Client::find($clientsWithPayments);//клиенты которые купили слитую ставку
            $this->sendFreeBetToClients($clientsWithPay, $betId);
        }

    }

    public function sendBetToClient($chatId, $bet_id, $client_id)
    {
        $username = Client::find($client_id)->tg_username;
        $bet = Bet::find($bet_id);
        $keyboard = [["Главное меню"], ["Статистика"], ["О проекте"]];
        $reply_markup = $this->telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
        $text = "Привет, " . $username . ". Ваша оплата получена успешно.Начало матча:" . $bet->start_date . ". Ставка: " . $bet->screen;
        $this->telegram->sendMessage(['chat_id' => $chatId, 'text' => $text, 'reply_markup' => $reply_markup]);
    }

    public function sendFreeBetToClients($clients, $betId)
    {
        $bet = Bet::find($betId);
        foreach ($clients as $item) {
            $keyboard = [["Главное меню"], ["Статистика"], ["О проекте"]];
            $reply_markup = $this->telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
            $text = "Привет, " . $item->tg_username . " В связи с тем что прошлый купленный вами платный прогноз не прошел, либо был возврат, вам доступный текущий платный прогноз бесплатно. Ставка: " . $bet->screen;
            $this->telegram->sendMessage(['chat_id' => $item->tg_chat_id, 'text' => $text, 'reply_markup' => $reply_markup]);
        }
    }
}
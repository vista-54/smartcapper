<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Library\Services\TelegramBot;

class TelegramBotServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Library\Services\TelegramBot', function ($app) {
            return new TelegramBot();
        });
    }
}

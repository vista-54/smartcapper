<?php

namespace App\Http\Controllers;

use App\Library\Services\TelegramBot;
use App\models\Bet;
use App\models\Client;
use App\models\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function success(Request $request, TelegramBot $telegramBot)
    {
        $data = $request->input();
        $user_chat_id = explode("b", $data['MERCHANT_ORDER_ID'])[0];
        $bet_id = explode("b", $data['MERCHANT_ORDER_ID'])[1];
        $payment = new Payment();
        $payment->transaction_id = $data['intid'];
        $payment->sum = $data['AMOUNT'];
        $payment->bet_id = $bet_id;
        $payment->client_id = Client::where(['tg_chat_id' => $user_chat_id])->get()[0]->id;
        $payment->status = "1";
        if ($payment->save()) {
            $telegramBot->sendBetToClient($user_chat_id, $bet_id, $payment->client_id);
            return "Pay has been successfully saved";
        } else {
            return "Unknow error";
        }
    }


}

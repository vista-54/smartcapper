<?php

namespace App\Http\Controllers;


use App\Library\Services\TelegramBot;

class TelegramBotController extends Controller
{

    public function index(TelegramBot $telegramBot)
    {
        $telegramBot->run();
    }


}

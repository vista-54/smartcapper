<?php

namespace App\Http\Controllers;

use App\Http\Requests\BetRequest;
use App\Library\Services\TelegramBot;
use App\models\Bet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Types\Integer;

class BetController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Bet $bet)
    {

        $bets = $bet->orderBy('id', 'desc')->get();
//        return view('bet.index', ['bets' => $bets]);
        return view('bet.index', compact('bets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('bet.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Bet $bet, BetRequest $request)
    {
        $bet->create($request->all());
        return redirect()->route('bet.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  Bet $bet
     * @return \Illuminate\Http\Response
     */
    public function show(Bet $bet)
    {
        return view('bet.show', compact('bet'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Bet $bet)
    {
        return view('bet.edit', compact('bet'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(BetRequest $request, Bet $bet)
    {
        $bet->update($request->all());
        return redirect()->route('bet.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param   $bet
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bet $bet)
    {
        $bet->delete();
        return redirect()->route('bet.index');
    }

    /**
     * @param $id
     * @param TelegramBot $telegramBot
     * @return \Illuminate\Http\RedirectResponse
     */
    public function publish($id, TelegramBot $telegramBot)
    {
        $telegramBot->sendNewBetToClients($id);
//        $bet = Bet::find($id);
//        $bet->published = 1;
//        $bet->save();
        return redirect()->route('bet.index');

    }
}

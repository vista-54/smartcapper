<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $guarded = ["*"];
    protected $fillable = ['tg_chat_id', 'tg_username', 'tg_first_name', 'tg_last_name'];

}

<?php

namespace App\models;

use Egulias\EmailValidator\Exception\AtextAfterCFWS;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bet extends Model
{
    use softDeletes;
    protected $guarded = ["*"];
    protected $fillable = ['title', 'body', 'screen', 'created_by', 'status', 'start_date', 'coefficient', 'published'];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($table) {
            $table->created_by = Auth::user()->id;
        });
    }


}

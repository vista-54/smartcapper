<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('transaction_id');
            $table->integer('client_id')->unsigned();
            $table->integer('bet_id')->unsigned();
            $table->integer('sum');
            $table->boolean('status');
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('bet_id')->references('id')->on('bets');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}

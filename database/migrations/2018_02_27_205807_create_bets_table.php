<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bets', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title');
            $table->text('body')->nullable();
            $table->integer('created_by')->unsigned();
            $table->text('screen');
            $table->enum('status', ['win', 'lose', 'refund', 'pending'])->default('pending');
            $table->string('start_date');
            $table->string('coefficient', '4');
            $table->tinyInteger('published')->default(0);
            $table->timestamps();
            $table->foreign('created_by')->references('id')->on('users');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bets');
    }
}

<?php

use Illuminate\Http\Request;
use \App\models\Bet;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('articles', function () {
    // If the Content-Type and Accept headers are set to 'application/json',
    // this will return a JSON structure. This will be cleaned up later.
    return "hellow";
});

Route::post('telegram-bot', "TelegramBotController@index");

Route::post('payment-success', "PaymentController@success");

Route::get('payment-error', function (Request $request) {

    return json_encode($request);
//    return "success";
});
